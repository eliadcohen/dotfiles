# Common Configuration

These are the common configurations used by one or more other operating system or device specific configurations. This reduces redundancy by maintained them in once place and sourcing and built in different ways depending on the need. It also allows others to directly source them into their own configurations if they prefer to use these that I maintain.
