# Bash Configuration

This is my main personal configuration that includes helpers for blogging, streaming, hosting virtual machines, and connecting to other systems.

## Setup and Installation

```
./setup
```

Running the `build` script is not necessary since I do that before committing to Git.
