
alias kali='ssh kali'
alias pico='ssh pico'

alias ignore="twitch chatoff"
alias focus="twitch focus"
alias flowstate="twitch flowstate"
alias casual="twitch casual"
alias casualstat="twitch casualstat"
alias chat="twitch chat"
alias chatnicks="twitch chatnicks"
alias status="twitch status"
alias nosoup4u="twitch ban"
alias raid="twitch raid"

alias c="clear"
alias cl="s /buffer clear"
alias x="exit"
alias sl="sl -e"
